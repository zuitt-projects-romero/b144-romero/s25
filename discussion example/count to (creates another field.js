db.scores.aggregate([
    { $match: {score: {$gte: 80}}},
    { $count: "passing_score"}
])