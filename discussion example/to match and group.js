db.fruits.aggregate(
[
    { $match: { onSale: true }  },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock"}}},
    { $project: {_id: 0 }}
]
)